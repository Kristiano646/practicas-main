export const Datos_accidentes=[ 
    { 
      Id: '1',
      Mes:'Enero',
      Numerador:'8',
      Denominador:'9',
      Resultados:'31',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '2',
      Mes:'Febrero ',
      Numerador:'9',
      Denominador:'10',
      Resultados:'28',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '3',
      Mes:'Marzo',
      Numerador:'9',
      Denominador:'10',
      Resultados:'31',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '4',
      Mes:'Abril',
      Numerador:'9',
      Denominador:'10',
      Resultados:'30',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '5',
      Mes:'Mayo',
      Numerador:'9',
      Denominador:'10',
      Resultados:'31',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '6',
      Mes:'Junio',
      Numerador:'9',
      Denominador:'10',
      Resultados:'30',
      Causas:'Inspecciones de instalaciones a los diferentes centros de trabajo',
      Acciones:'Realizar inspecciones programadas y no programadas'     
    },
    { 
      Id: '7',
      Mes:'Julio',
      Numerador:'9',
      Denominador:'10',
      Resultados:'',
      Causas:'',
      Acciones:''     
    },
    { 
      Id: '8',
      Mes:'Agosto',
      Numerador:'',
      Denominador:'',
      Resultados:'',
      Causas:'',
      Acciones:''     
    },
    { 
      Id: '9',
      Mes:'Septiembre',
      Numerador:'',
      Denominador:'',
      Resultados:'100',
      Causas:'',
      Acciones:''     
    },
    { 
      Id: '10',
      Mes:'Octubre',
      Numerador:'',
      Denominador:'',
      Resultados:'',
      Causas:'',
      Acciones:''     
    },
    { 
      Id: '11',
      Mes:'Noviembre',
      Numerador:'',
      Denominador:'',
      Resultados:'',
      Causas:'',
      Acciones:''     
    },
    { 
      Id: '12',
      Mes:'Diciembre',
      Numerador:'',
      Denominador:'',
      Resultados:'',
      Causas:'',
      Acciones:''     
    },
    
    ]
    